#include "Parser.h"
#include "Block.h"
#include <sstream>
Parser::Parser() : got_out(false)
{}
bool Parser::CheckId(std::string id)
{
	if ((id.find('.') != std::string::npos) || (id.find(',') != std::string::npos) || 
			(id.find('/') != std::string::npos) || std::stoi(id) < 0)
		return false;
	return true;
}
bool Parser::CheckArrow(std::string arrow)
{
	std::string temp = "->";
	if (arrow != temp)
		return false;
	return true;
}
bool Parser::CheckOperation(std::string operation)
{
	if ((operation != "readfile") && (operation != "writefile") && (operation != "grep") &&
				(operation != "sort") && (operation != "replace") && (operation != "dump"))
		return false;
	return true;
}
Block* Parser::GetBlockByName(std::string operation)
{
	if (operation == "readfile")
		return new ReadfileBlock();
	if (operation == "writefile")
		return new WritefileBlock();
	if (operation == "grep")
		return new GrepBlock();
	if (operation == "sort")
		return new SortBlock();
	if (operation == "replace")
		return new ReplaceBlock();
	if (operation == "dump")
		return new DumpBlock();
}
bool Parser::CheckCollisions(std::vector<Block*> * flow)
{
	if ((*flow)[0]->type() != 0)
		return false;
	if ((*flow)[flow->size() - 1]->type() != 1)
		return false;
	int r = 0;
	int w = 0;
	for (int i = 0; i < flow->size(); i++)
	{
		if ((*flow)[i]->type() == 0)
			r++;
		if ((*flow)[i]->type() == 1)
			w++;
	}
	if ((r != 1) || (w != 1))
		return false;
	return true;
}
std::vector<Block*> * Parser::GetFlow()
{
	return &flow;
}
void Parser::ParseKeys(char *argv[])
{
	std::stringstream ss;
	std::string str;
	ss << argv[2] << " " <<argv[3];
	ss >> str;
	if (str == "-i")
	{
		ss >> str;
		scheme[-1] = GetBlockByName("readfile");
		scheme[-1]->set_arg(str);
		if (Keys == 6)
		{
			ss.clear();
			ss.str("");
			ss << argv[4] << " " << argv[5];
			ss >> str;
			if (str == "-o")
			{
				ss >> str;
				scheme[-2] = GetBlockByName("writefile");
				scheme[-2]->set_arg(str);
				got_out = true;
			}
			else throw std::invalid_argument("Incorrect argv[]");
		}
		flow.push_back(scheme[-1]);
		return;
	}
	else if (str == "-o")
	{
		got_out = true;
		ss >> str;
		scheme[-2] = GetBlockByName("writefile");
		scheme[-2]->set_arg(str);
		if (Keys == 6)
		{
			ss.clear();
			ss.str("");
			ss << argv[4] << " " << argv[5];
			ss >> str;
			if (str == "-i")
			{
				ss >> str;
				scheme[-1] = GetBlockByName("readfile");
				scheme[-1]->set_arg(str);
				flow.push_back(scheme[-1]);
			}
			else throw std::invalid_argument("Incorrect argv[]");
		}
		return;
	}
	throw std::invalid_argument("Incorrect argv[]");
}
void Parser::ParseInstructions(std::ifstream & fin)
{
	std::stringstream ss;
	std::string line;
	std::string s_id;
	std::string arrow;
	std::string eq;
	std::string operation;
	std::string argument;
	std::string buf_arg;
	std::string err;
	short id = -10;
	std::vector<short> used_ids;
/////////////////parsing blocks///////////////////////////////////////
	std::getline(fin, line);
	if (line != "desc")
		throw std::runtime_error("Missing \"desc\"");
	while (1)
	{
		ss.clear();
		ss.str("");
		argument = "";
		buf_arg = "";
		std::getline(fin, line);
		if (line == "csed")
			break;
		ss << line;
		ss >> s_id;
		if (!CheckId(s_id))
			throw std::invalid_argument("Incorrect Instruction");
		ss >> eq;
		if (eq != "=")
			throw std::invalid_argument("Incorrect Instruction");
		ss >> operation;
		if (!CheckOperation(operation))
			throw std::invalid_argument("Incorrect Instruction");
		ss >> argument;
		ss >> buf_arg;
		if (!buf_arg.empty())
		{
			argument += " ";
			argument += buf_arg;
		}
		ss >> err;
		if (!err.empty())
		{
			throw std::invalid_argument("Too many arguments");
			return;
		}
		id = std::stoi(s_id);
		if (find(used_ids.begin(), used_ids.end(), id) != used_ids.end())
			throw std::invalid_argument("Multiple Definitions for Single Id");
		used_ids.push_back(id);
		scheme[id] = GetBlockByName(operation);
		scheme[id]->set_arg(argument);
	}
	/////////////////parsing flow///////////////////////////////////////
	std::map<short, Block*>::iterator it;
	ss.clear();
	ss.str("");
	if (!fin.good())
		throw std::invalid_argument("Incorrect Input");
	std::getline(fin, line);
	ss << line;
	while (ss.rdbuf()->in_avail() != 0)
	{
		ss >> s_id;
		if (!CheckId(s_id))
			throw std::invalid_argument("Incorrect Instruction");
		id = std::stoi(s_id);
		it = scheme.find(id);
		if (it == scheme.end())
			throw std::invalid_argument("No Such Id");
		ss >> arrow;
		if (!CheckArrow(arrow))
			throw std::invalid_argument("Incorrect Instruction");
		flow.push_back(scheme[id]);
	}
	if (got_out)
		flow.push_back(scheme[-2]);
	if (!CheckCollisions(&flow))
		throw std::invalid_argument("Collisions between instructions");
	return;
}
Parser::~Parser()
{
	for (auto it = scheme.begin(); it != scheme.end(); ++it)
	{
		delete (*it).second;
	}
}