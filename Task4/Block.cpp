#include "Block.h"
#include <iostream>
#include <fstream>
#include <sstream>
void Block::set_arg(std::string arg)
{
	this->arg = arg;
}
ReadfileBlock::ReadfileBlock() : Block()
{}
void ReadfileBlock::execute(std::list<std::string> & lines)
{
	if (arg.empty())
		throw std::invalid_argument("Wrong Arguments");
	if (arg.find(' ') != std::string::npos)
		throw std::invalid_argument("Too Many Arguments");
	std::ifstream fin;
	fin.open(arg);
	if (!fin)
		throw std::runtime_error("Cannot open file");
	if (!fin.good())
		throw std::runtime_error("Empty file");
	std::string line;
	while (fin.good())
	{
		std::getline(fin, line);
		lines.push_back(line);
	}
	fin.close();
}
char ReadfileBlock::type()
{
	return 0;
}
WritefileBlock::WritefileBlock() : Block()
{}
void WritefileBlock::execute(std::list<std::string> & lines)
{
	if (arg.empty())
		throw std::invalid_argument("Wrong Arguments");
	if (arg.find(' ') != std::string::npos)
		throw std::invalid_argument("Too Many Arguments");
	std::ofstream fout;
	fout.open(arg);
	if (!fout)
		throw std::runtime_error("Cannot open file");
	while (!lines.empty())
	{
		fout << lines.front() << std::endl;
		lines.pop_front();
	}
	fout.close();
}
char WritefileBlock::type()
{
	return 1;
}
GrepBlock::GrepBlock() : Block()
{}
void GrepBlock::execute(std::list<std::string> & lines)
{
	if (arg.empty())
		throw std::invalid_argument("Wrong Arguments");
	if (arg.find(' ') != std::string::npos)
		throw std::invalid_argument("Too Many Arguments");
	for (auto itr = lines.begin(); itr != lines.end(); ++itr) {
		if (itr->find(arg) == std::string::npos)
		{
			itr = lines.erase(itr);
			if (lines.empty())
				break;
		}
	}
}
char GrepBlock::type()
{
	return 2;
}
SortBlock::SortBlock() : Block()
{}
void SortBlock::execute(std::list<std::string> & lines)
{
	if (!arg.empty())
		throw std::invalid_argument("Too Many Arguments");
	lines.sort();
}
char SortBlock::type()
{
	return 3;
}
ReplaceBlock::ReplaceBlock() : Block()
{}
void ReplaceBlock::execute(std::list<std::string> & lines)
{
	if (arg.empty())
		throw std::invalid_argument("Wrong Arguments");
	if (arg.find(' ') == std::string::npos)
		throw std::invalid_argument("Not Enough Arguments");
	std::stringstream ss;
	std::string a1;
	std::string a2;
	ss << arg;
	ss >> a1;
	ss >> a2;
	size_t start_pos = 0;
	size_t end_pos = 0;
	for (auto itr = lines.begin(); itr != lines.end(); ++itr) {
		start_pos = 0;
		while ((start_pos = (*itr).find(a1, start_pos)) != std::string::npos) {
			end_pos = start_pos + a1.length();
			(*itr).replace(start_pos, end_pos, a2);
			start_pos += a2.length();
		}
	}
}
char ReplaceBlock::type()
{
	return 4;
}
DumpBlock::DumpBlock() : Block()
{}
void DumpBlock::execute(std::list<std::string> & lines)
{
	if (arg.empty())
		throw std::invalid_argument("Wrong Arguments");
	if (arg.find(' ') != std::string::npos)
		throw std::invalid_argument("Too Many Arguments");
	std::ofstream fout;
	fout.open(arg);
	if (!fout)
		throw std::runtime_error("Cannot open file");
	for (auto itr = lines.begin(); itr != lines.end(); ++itr) {
		fout << (*itr) << std::endl;
	}
	fout.close();
}
char DumpBlock::type()
{
	return 5;
}