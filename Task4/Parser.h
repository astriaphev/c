#ifndef PARSER_H
#define PARSER_H
#include <map>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include "Workflow.h"
class Block;
class Parser
{
private:
	std::map <short, Block*> scheme;
	std::vector<Block*> flow;
	bool got_out;
public:
	Parser();
	int Keys;
	bool CheckId(std::string id);
	bool CheckArrow(std::string arrow);
	bool CheckOperation(std::string operation);
	Block* GetBlockByName(std::string operation);
	bool CheckCollisions(std::vector<Block*> * flow);
	std::vector<Block*> * GetFlow();
	void ParseKeys(char *argv[]);
	void ParseInstructions(std::ifstream & fin);
	~Parser();
};
#endif