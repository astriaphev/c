#ifndef BLOCK_H
#define BLOCK_H
#include <string>
#include <list>
class Block
{
public:
	virtual void execute(std::list<std::string> & lines) = 0;
	virtual char type() = 0;
	std::string arg;
	void set_arg(std::string arg);
protected:
	Block()
	{
	}
};
class ReadfileBlock : public Block
{
public:
	ReadfileBlock();
	void execute(std::list<std::string> & lines);
	char type();
};
class WritefileBlock : public Block
{
public:
	WritefileBlock();
	void execute(std::list<std::string> & lines);
	char type();
};
class GrepBlock : public Block
{
public:
	GrepBlock();
	void execute(std::list<std::string> & lines);
	char type();
};
class SortBlock : public Block
{
public:
	SortBlock();
	void execute(std::list<std::string> & lines);
	char type();
};
class ReplaceBlock : public Block
{
public:
	ReplaceBlock();
	void execute(std::list<std::string> & lines);
	char type();
};
class DumpBlock : public Block
{
public:
	DumpBlock();
	void execute(std::list<std::string> & lines);
	char type();
};
#endif