#include "Workflow.h"
Workflow::Workflow()
{
	p = new Parser();
	e = new FlowExecutor();
}
void Workflow::SetArgc(int argc)
{
	p->Keys = argc;
}
void Workflow::SendKeys(char *argv[])
{
	p->ParseKeys(argv);
}
void Workflow::Iniciate(char * filename)
{
	std::ifstream fin;
	fin.open(filename);
	if (!fin)
		throw std::invalid_argument("Cannot open file");
	p->ParseInstructions(fin);
	fin.close();
	std::vector<Block*> * flow = p->GetFlow();
	e->ExecuteFlow(flow);
}
Workflow::~Workflow()
{
	delete p;
	delete e;
}