#include "Workflow.h"
int main(int argc, char *argv[])
{
	Workflow wf = Workflow();
	try {
		if (argc == 1)
			throw std::runtime_error("No Input file");
		if ((argc == 4) || (argc == 6))
		{
			wf.SetArgc(argc);
			wf.SendKeys(argv);
		}
		else if (argc != 2)
			throw std::invalid_argument("Incorrect argv[]");
		wf.Iniciate(argv[1]);
	}
	catch (const std::invalid_argument& err) {
		std::cout << "Error: " << err.what() << std::endl;;
		system("pause");
		return 1;
	}
	catch (const std::runtime_error& err) {
		std::cout << "Runtime Error: " << err.what() << std::endl;
		system("pause");
		return 2;
	}
	system("pause");
	return 0;
}