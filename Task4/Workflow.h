#ifndef WORKFLOW_H
#define WORKFLOW_H
#include <iostream>
#include <string>
#include <fstream>
#include "Parser.h"
#include "FlowExecutor.h"
class Parser;
class FlowExecutor;
class Workflow
{
private:
	Parser * p;
	FlowExecutor * e;
public:
	Workflow();
	void SendKeys(char *argv[]);
	void SetArgc(int argc);
	void Iniciate(char * filename);
	~Workflow();
};
#endif
