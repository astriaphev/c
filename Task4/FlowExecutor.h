#ifndef FLOW_EXECUTOR
#define FLOW_EXECUTOR
#include <list>
#include <string>
#include <vector>
class Block;
class FlowExecutor
{
private:
	std::list<std::string> lines;
public:
	FlowExecutor();
	void ExecuteFlow(std::vector<Block*> * flow);
	friend class ReadfileBlock;
};
#endif