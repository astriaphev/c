#ifndef COMMAND_H
#define COMMAND_H
#include "LifeGame.h"
class Model;
class Command
{
public:
	virtual void execute() = 0;
	int i;
	std::string arg;
	void set_arg(std::string arg);
protected:
	Model* model;
	Command(Model & m)
	{
		this->model = & m;
	}
};
class ResetCommand : public Command
{
public:
	ResetCommand(Model &m);
	void execute();
};
class SetCommand : public Command
{
public:
	SetCommand(Model &m);
	void execute();
};
class ClearCommand : public Command
{
public:
	ClearCommand(Model &m);
	void execute();
};
class StepCommand : public Command
{
public:
	StepCommand(Model &m);
	void execute();
};
class BackCommand : public Command
{
public:
	BackCommand(Model &m);
	void execute();
};
class SaveCommand : public Command
{
public:
	SaveCommand(Model &m);
	void execute();
};
class LoadCommand : public Command
{
public:
	LoadCommand(Model &m);
	void execute();
};
#endif