﻿#include "View.h"
#include "Reciever.h"
View::View(Model & m)
{
	model = &m;
}
void View::GameOver()
{
	if (!model->is_over())
		return;
	std::cout << "\n\
  _|_|_|    _|_|    _|      _|  _|_|_|_|        _|_|    _|      _|  _|_|_|_|  _|_|_|    \
_|        _|    _|  _|_|  _|_|  _|            _|    _|  _|      _|  _|        _|    _|  \
_|  _|_|  _|_|_|_|  _|  _|  _|  _|_|_|        _|    _|  _|      _|  _|_|_|    _|_|_|    \
_|    _|  _|    _|  _|      _|  _|            _|    _|    _|  _|    _|        _|    _|  \
  _|_|_|  _|    _|  _|      _|  _|_|_|_|        _|_|        _|      _|_|_|_|  _|    _|" << std::endl;
}
void View::Update()
{
	std::vector<bool> field = model->StatesForView();
	int i, j;
	bool cell;
	//std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	std::cout << " 0 1 2 3 4 5 6 7 8 9" << std::endl;
	char letter = 'A';
	for (i = 0; i < 10; i++)
	{
		std::cout << letter;
		letter++;
		for (j = 0; j < 10; j++)
		{
			cell = field[i * 10 + j];
			if (cell == true)
				std::cout << "* ";
			else std::cout << ". ";
		}
		std::cout << std::endl;
	}
	std::cout << "Iterations:" << model->get_iterations() << std::endl;
	std::cout << "Enter Command:  ";
}