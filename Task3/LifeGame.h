#ifndef LIFE_GAME_H
#define LIFE_GAME_H
#include <iostream>
#include <vector>

class Reciever;
class View;

class Cell
{
private:
	std::vector<bool> history;
	bool current_state;
	char neighbors;
public:
	Cell();
	friend class Model;
};
class Model
{
private:
	Cell field[10 * 10];
	int iterations;
	bool game_over;
	int leap(int n);
	char B2C(bool n);
	bool C2B(char n);
	void UpdateNeighbors();
	bool UpdateField();
public:
	std::vector<bool> StatesForView();
	Model();
	bool is_over();
	int get_iterations();
	void reset();
	void set(int X, int Y);
	void clear(int X, int Y);
	void step();
	void back();
	std::string save();
	void load(std::ifstream & fin);
};
class LifeGame
{
private:
	Model * m;
	View * view;
	Reciever * rec;
public:
	LifeGame();
	void Start();
	~LifeGame();
};
#endif