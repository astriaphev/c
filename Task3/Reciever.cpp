#include <string>
#include <sstream>
#include "Reciever.h"
#include "View.h"
Reciever::Reciever(Model & m)
{
	commands["reset"] = new ResetCommand(m);
	commands["set"] = new SetCommand(m);
	commands["clear"] = new ClearCommand(m);
	commands["step"] = new StepCommand(m);
	commands["back"] = new BackCommand(m);
	commands["save"] = new SaveCommand(m);
	commands["load"] = new LoadCommand(m);
	view =  new View(m);
}
void Reciever::ParseAndExecute()
{
	std::stringstream ss;
	std::string line;
	std::string operation;
	std::string arg;
	std::string err;
	std::getline(std::cin, line);
	ss << line;
	ss >> operation;
	ss >> arg;
	ss >> err;
	if (!err.empty())
	{
		std::cout << "Too many arguments" << std::endl;
		return;
	}
	if (commands.count(operation) == 0)
	{
		std::cout << "Wrond Command" << std::endl;
		return;
	}
	system("cls");
	commands[operation]->set_arg(arg);
	commands[operation]->execute();
	view->Update();
	view->GameOver();
}
Reciever::~Reciever()
{
	delete commands["reset"];
	delete commands["set"];
	delete commands["clear"];
	delete commands["step"];
	delete commands["back"];
	delete commands["save"];
	delete commands["load"];
	delete view;
}