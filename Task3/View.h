﻿#ifndef VIEW_H
#define VIEW_H
class Model;
class View
{
private:
	Model * model;
public:
	View(Model &m);
	void GameOver();
	void Update();
};
#endif