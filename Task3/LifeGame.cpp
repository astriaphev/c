#include "LifeGame.h"
#include "Reciever.h"
#include "View.h"
#include <fstream>
#include <string>
Cell::Cell() :
	current_state(false),
	neighbors(0)
{}
Model::Model()
{
	int i;
	this->iterations = 0;
	this->game_over = false;
	for (i = 0; i < 100; i++)
	{
		field[i].current_state = false;
		field[i].history.push_back(false);
	}
	return;
}
int Model::leap(int n) {
	return ((n + 10) % 10);
}
char Model::B2C(bool n)
{
	if (n == true)
		return '1';
	else return '0';
}
bool Model::C2B(char n)
{
	if (n == '1')
		return true;
	if (n == '0')
		return false;
	return false;
}
void Model::UpdateNeighbors()
{
	int i, j;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 10; j++)
		{
			field[i * 10 + j].neighbors = 0;
			if (field[leap(i - 1) * 10 + j].current_state == true)
				field[i * 10 + j].neighbors++;
			if (field[leap(i - 1) * 10 + leap(j - 1)].current_state == true)
				field[i * 10 + j].neighbors++;
			if (field[leap(i - 1) * 10 + leap(j + 1)].current_state == true)
				field[i * 10 + j].neighbors++;
			if (field[i * 10 + leap(j - 1)].current_state == true)
				field[i * 10 + j].neighbors++;
			if (field[i * 10 + leap(j + 1)].current_state == true)
				field[i * 10 + j].neighbors++;
			if (field[leap(i + 1) * 10 + j].current_state == true)
				field[i * 10 + j].neighbors++;
			if (field[leap(i + 1) * 10 + leap(j - 1)].current_state == true)
				field[i * 10 + j].neighbors++;
			if (field[leap(i + 1) * 10 + leap(j + 1)].current_state == true)
				field[i * 10 + j].neighbors++;
		}
	}
}
bool Model::UpdateField()
{
	int i, j;
	int k = 0;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 10; j++)
		{
			if ((field[i * 10 + j].current_state == false) && (field[i * 10 + j].neighbors == 3))
			{
				field[i * 10 + j].current_state = true;
				field[i * 10 + j].history.push_back(field[i * 10 + j].current_state);
				k++;
				continue;
			}
			if ((field[i * 10 + j].current_state == true) && (field[i * 10 + j].neighbors < 2))
			{
				field[i * 10 + j].current_state = false;
				field[i * 10 + j].history.push_back(field[i * 10 + j].current_state);
				k++;
				continue;
			}
			if ((field[i * 10 + j].current_state == true) && (field[i * 10 + j].neighbors > 3))
			{
				field[i * 10 + j].current_state = false;
				field[i * 10 + j].history.push_back(field[i * 10 + j].current_state);
				k++;
				continue;
			}
			field[i * 10 + j].history.push_back(field[i * 10 + j].current_state);
		}
	}
	if (k != 0) return false;
	else return true;
}
std::vector<bool> Model::StatesForView()
{
	int i;
	std::vector<bool> res;
	for (i = 0; i < 100; i++)
		res.push_back(field[i].current_state);
	return res;
}
bool Model::is_over()
{
	return game_over;
}
int Model::get_iterations()
{
	return iterations;
}
void Model::reset()
{
	int i;
	iterations = 0;
	for (i = 0; i < 100; i++)
		field[i].current_state = false;
}
void Model::set(int X, int Y)
{
	field[X * 10 + Y].current_state = true;
	field[X * 10 + Y].history.back() = true;
	return;
}
void Model::clear(int X, int Y)
{
	field[X * 10 + Y].current_state = false;
	field[X * 10 + Y].history.back() = false;
}
void Model::step()
{
	UpdateNeighbors();
	game_over = UpdateField();
	iterations++;
}
void Model::back()
{
	if (iterations == 0)
		return;
	int i, j;
	for (i = 0; i < 10; i++)
		for (j = 0; j < 10; j++)
		{
			field[i * 10 + j].history.pop_back();
			field[i * 10 + j].current_state = field[i * 10 + j].history.back();
		}
	iterations--;
}
std::string Model::save()
{
	std::string res;
	int i;
	for (i = 0; i < 100; i++)
		res += B2C(field[i].current_state);
	return res;
}
void Model::load(std::ifstream & fin)
{
	std::string data;
	fin >> data;
	int i;
	bool buf;
	for (i = 0; i < 100; i++)
	{
		buf = C2B(data[i]);
		field[i].current_state = buf;
		field[i].history.back() = buf;
	}
}
LifeGame::LifeGame()
{
	m = new Model();
	view = new View(*m);
	rec = new Reciever(*m);
}
void LifeGame::Start()
{
	while (!m->is_over())
	{
		rec->ParseAndExecute();
	}
}
LifeGame::~LifeGame()
{
	delete m;
	delete view;
	delete rec;
}