#include "Command.h"
#include <fstream>
#include <string>
void Command::set_arg(std::string arg)
{
	this->arg = arg;
}
ResetCommand::ResetCommand(Model &m) : Command(m)
{}
void ResetCommand::execute()
{
	if (!arg.empty())
	{
		std::cout << "Too Many Arguments" << std::endl;
		return;
	}
	model->reset();

	return;
}
SetCommand::SetCommand(Model & m) : Command(m)
{}
void SetCommand::execute()
{
	if (arg.size() != 2)
	{
		std::cout << "Wrong Arguments" << std::endl;
		return;
	}
	if(arg[0] == NULL || arg[1] == NULL)
	{
		std::cout << "Wrong Arguments" << std::endl;
		return;
	}
	char X, Y;
	X = arg[0];
	Y = arg[1];
	if (X < 'A' || X > 'J' || Y < '0' || Y > '9') {
		std::cout << "Wrong command" << std::endl;
		return;
	}
	X = arg[0] - 'A';
	Y = arg[1] - '0';
	model->set(X, Y);
	return;
}
ClearCommand::ClearCommand(Model & m) : Command(m)
{}
void ClearCommand::execute()
{
	if (arg.size() != 2)
	{
		std::cout << "Wrong Arguments" << std::endl;
		return;
	}
	if (arg[0] == NULL || arg[1] == NULL)
	{
		std::cout << "Wrong Arguments" << std::endl;
		return;
	}
	char X, Y;
	X = arg[0];
	Y = arg[1];
	if (X < 'A' || X > 'J' || Y < '0' || Y > '9') {
		std::cout << "Wrong command" << std::endl;
		return;
	}
	X = arg[0] - 'A';
	Y = arg[1] - '0';
	model->clear(X, Y);
	return;
}
StepCommand::StepCommand(Model & m) : Command(m)
{}
void StepCommand::execute()
{
	int i;
	int n = 1;
	if (arg[0] != NULL)
		n = std::stoi(arg);
	for (i = 0; i < n; i++)
	{
		model->step();
	}
	return;
}
BackCommand::BackCommand(Model & m) : Command(m)
{}
void BackCommand::execute()
{
	if (!arg.empty())
	{
		std::cout << "Too Many Arguments" << std::endl;
		return;
	}
	model->back();
	return;
}
SaveCommand::SaveCommand(Model & m) : Command(m)
{}
void SaveCommand::execute()
{
	if (arg.empty() == true)
	{
		std::cout << "Wrong Arguments" << std::endl;
		return;
	}
	std::string res = model->save();
	std::ofstream fout;
	fout.open(arg);
	fout << res;
	fout.close();
	return;
}
LoadCommand::LoadCommand(Model & m) : Command(m)
{}
void LoadCommand::execute()
{
	if ((arg.empty() == true) || (arg.find(' ') != std::string::npos))
	{
		std::cout << "Wrong Arguments" << std::endl;
		return;
	}
	std::ifstream fin;
	fin.open(arg);
	if (!fin) {
		std::cout << "Cannot open file" << std::endl;
		return;
	}
	model->load(fin);
	fin.close();
	return;
}