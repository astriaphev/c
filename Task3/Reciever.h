#ifndef RECIEVER_H
#define RECIEVER_H
#include "Command.h"
#include <map>
class View;
class Reciever
{
private:
	std::map <std::string, Command*> commands;
	View * view;
public:
	Reciever(Model & m);
	void ParseAndExecute();
	~Reciever();
};
#endif