#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include "sort.h"
int main(int argc, char* argv[])
{
	using namespace std;
	ifstream in;
	ofstream out;
	in.open(argv[1]);
	out.open(argv[2]);
	string str;
	list<string> l;
	while (getline(in, str))
		l.push_back(str);
	sort::sort_strings(l);
	while (!l.empty()) {
		out << l.front() << endl;
		l.pop_front();
	}
	in.close();
	out.close();
	//system("pause");
	return 0;
}