﻿#include "Date.h"
#include "DateInterval.h"
	Date::Date(const Date &src) { // конструктор копии
		this->year = src.year;
		this->month = src.month;
		this->day = src.day;
		this->hour = src.hour;
		this->minute = src.minute;
		this->second = src.second;
	}
	Date::Date() { // конструктор по умолчанию - текущая дата и время UTC
		time_t t = time(0);
		tm *time = gmtime(&t);
		this->second = time->tm_sec;
		this->minute = time->tm_min;
		this->hour = time->tm_hour;
		this->day = time->tm_mday;
		this->month = (Month)(time->tm_mon + 1);
		this->year = time->tm_year + 1900;
	}
	Date::Date(int year, Month month, int day, int hour, int minute, int second) { // полный конструктор
		this->second = second;
		this->minute = minute;
		this->hour = hour;
		this->day = day;
		this->month = month;
		this->year = year;
		Normalize(this->year, this->month, this->day, this->hour, this->minute, this->second);
	}
	Date::Date(unsigned int y, Month m, unsigned int d) { // создает объект с временем 0 ч 0 м 0 сек
		this->year = y;
		this->month = m;
		this->day = d;
		this->hour = 0;
		this->minute = 0;
		this->second = 0;
	}
	Date::Date(unsigned int hrs, unsigned int min, unsigned int sec) { // создает объект с указанным временем и текущей датой
		time_t t = time(0);
		tm *time = gmtime(&t);
		this->second = sec;
		this->minute = min;
		this->hour = hrs;
		this->day = time->tm_mday;
		this->month = (Month)(time->tm_mon + 1);
		this->year = time->tm_year + 1900;
	}
	void Date::Normalize(int &year, Month &month, int &day, int &hour, int &minute, int &second) { // исправление некорректных данных
		int min_shift = 0;
		int hour_shift = 0;
		int day_shift = 0;
		int OK = 0;
		if (second > 59)
		{
			min_shift = second / 60;
			second = second % 60;
		}
		minute += min_shift;
		if (minute > 59)
		{
			hour_shift = minute / 60;
			minute = minute % 60;
		}
		hour += hour_shift;
		if (hour > 23)
		{
			day_shift = hour / 24;
			hour = hour % 24;
		}
		day += day_shift;
		while (!OK) {
			if ((month == Jan) || (month == Mar) || (month == May) || (month == Jul) || (month == Aug) || (month == Oct))
			{
				if (day <= 31) { OK++; continue; }
				day = day - 31;
				month = (Month)(month + 1);
			}
			if ((month == Apr) || (month == Jun) || (month == Sep) || (month == Nov))
			{
				if (day <= 30) { OK++; continue; }
				day = day - 30;
				month = (Month)(month + 1);
			}
			if (month == Feb)
			{
				if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
				{
					if (day <= 29) { OK++; continue; }
					day = day - 29;
					month = (Month)(month + 1);
				}
				else
				{
					if (day <= 28) { OK++; continue; }
					day = day - 28;
					month = (Month)(month + 1);
				}
			}
			if (month == Dec)
			{
				if (day <= 31) { OK++; continue; }
				day = day - 31;
				month = Jan;
				year++;
			}
		}
		if ((year > 9999) || (year <= 0))
		{
			std::cout << "Max year is 9999 AD, min year is 1 AD" << std::endl;
			system("pause");
			exit(0);
		}
	}
	void Date::LimitDays()
	{
		if (this->day <= 28)
			return;
		if (this->month == Feb)
		{
			if (((this->year % 4 == 0) && (this->year % 100 != 0)) || (this->year % 400 == 0))
				this->day = 29;
			else this->day = 28;
			return;
		}
		if (this->day == 31)
			if ((month == Apr) || (month == Jun) || (month == Sep) || (month == Nov))
				this->day = 30;
		return;

	}
	void Date::MinusNormalize(int delta, Date cpy, char mode)
	{
		char OK = 0;
		switch (mode) {
		case 1:
		{
			if (this->second + delta < 0)
			{
				if (delta % 60 == 0)
					delta = delta / 60;
				else
				{
					this->second = 60 + this->second + (delta % 60);
					delta = delta / 60 - 1;
				}
			}
			else { this->second += delta;  break; }
		}
		case 2:
		{
			if (this->minute + delta < 0)
			{
				if (delta % 60 == 0)
					delta = delta / 60;
				else
				{
					this->minute = 60 + this->minute + (delta % 60);
					delta = delta / 60 - 1;
				}
			}
			else { this->minute += delta;  break; }
		}
		case 3:
		{
			if (this->hour + delta < 0)
			{
				if (delta % 24 == 0)
					delta = delta / 24;
				else
				{
					this->hour = 24 + this->hour + (delta % 24);
					delta = delta / 24 - 1;
				}
			}
			else { this->hour += delta;  break; }
		}
		case 4:
		{
			OK = 0;
			if (this->day + delta <= 0)
			{
				this->day += delta;
				while (!OK)
				{
					if ((this->month - 1 == Jan) || (this->month - 1 == Mar) || (this->month - 1 == May) || (this->month - 1 == Jul) || (this->month - 1 == Aug) || (this->month - 1 == Oct))
					{
						if (this->day > 0) { OK++; continue; }
						this->day += 31;
						this->month = (Month)(this->month - 1);
					}
					if ((this->month - 1 == Apr) || (this->month - 1 == Jun) || (this->month - 1 == Sep) || (this->month - 1 == Nov))
					{
						if (this->day > 0) { OK++; continue; }
						this->day += 30;
						this->month = (Month)(this->month - 1);
					}
					if (this->month - 1 == Feb)
					{
						if (((this->year % 4 == 0) && (this->year % 100 != 0)) || (this->year % 400 == 0))
						{
							if (this->day > 0) { OK++; continue; }
							this->day += 29;
							this->month = (Month)(this->month - 1);
						}
						else
						{
							if (this->day > 0) { OK++; continue; }
							this->day += 28;
							this->month = (Month)(this->month - 1);
						}
					}
					if (this->month == Jan)
					{
						if (this->day > 0) { OK++; continue; }
						this->year--;
						this->month = Dec;
						this->day += 31;
					}
				}
			}
			else { this->day += delta; break; }
			break;
		}
		case 5:
		{
			OK = 0;
			while (!OK)
			{
				if (this->month + delta > 0)
				{
					this->month = (Month)(this->month + delta);
					delta = 0;
					LimitDays();
					OK++;
					continue;
				}
				else
				{
					delta = delta + this->month;
					this->month = Dec;
					this->year--;
					continue;
				}
			}
		}
		case 6:
		{
			this->year += delta;
			LimitDays();
			break;
		}
		}
		return;
	}
	Date Date::addYears(int plus) { // создание копии в котором увеличено количество лет
		if (plus < 0)
		{
			Date cpy(*this);
			cpy.MinusNormalize(plus, cpy, 6);
			return cpy;
		}
		else
		{
			Date cpy(this->year + plus, this->month, this->day, this->hour, this->minute, this->second);
			return cpy;
		}
	}
	Date Date::addMonths(int plus) { // создание копии в котором увеличено количество месяцев
		if (plus < 0)
		{
			Date cpy(*this);
			cpy.MinusNormalize(plus, cpy, 5);
			return cpy;
		}
		else
		{
			int y = this->year;
			if ((Month)(this->month + plus) > 12)
				y = (this->month + plus) / 12 + this->year;
			Date cpy(y, (Month)((this->month + plus) % 12), this->day, this->hour, this->minute, this->second);
			return cpy;
		}
	}
	Date Date::addDays(int plus) { // создание копии в котором увеличено количество дней
		if (plus < 0)
		{
			Date cpy(*this);
			cpy.MinusNormalize(plus, cpy, 4);
			return cpy;
		}
		else
		{
			Date cpy(this->year, this->month, this->day + plus, this->hour, this->minute, this->second);
			return cpy;
		}
	}
	Date Date::addHours(int plus) { // создание копии в котором увеличено количество часов
		if (plus < 0)
		{
			Date cpy(*this);
			cpy.MinusNormalize(plus, cpy, 3);
			return cpy;
		}
		else
		{
			Date cpy(this->year, this->month, this->day, this->hour + plus, this->minute, this->second);
			return cpy;
		}
	}
	Date Date::addMinutes(int plus) { // создание копии в котором увеличено количество минут
		if (plus < 0)
		{
			Date cpy(*this);
			cpy.MinusNormalize(plus, cpy, 2);
			return cpy;
		}
		else
		{
			Date cpy(this->year, this->month, this->day, this->hour, this->minute + plus, this->second);
			return cpy;
		}
	}
	Date Date::addSeconds(int plus) { // создание копии в котором увеличено количество секунд
		if (plus < 0)
		{
			Date cpy(*this);
			cpy.MinusNormalize(plus, cpy, 1);
			return cpy;
		}
		else
		{
			Date cpy(this->year, this->month, this->day, this->hour, this->minute, this->second + plus);
			return cpy;
		}
	}
	std::string Date::toString() { // возвращение строкового представления даты
		std::string res = std::to_string(this->year);
		res += "-";
		res += std::to_string(this->month);
		res += "-";
		res += std::to_string(this->day);
		res += " ";
		res += std::to_string(this->hour);
		res += "::";
		res += std::to_string(this->minute);
		res += "::";
		res += std::to_string(this->second);
		return res;
	}
	bool operator>(Date & left, Date & right) {
		if (left.GetYear() > right.GetYear()) return true;
		else if (left.GetYear() < right.GetYear()) return false;
		if (left.GetMonth() > right.GetMonth()) return true;
		else if (left.GetMonth() < right.GetMonth()) return false;
		if (left.GetDay() > right.GetDay()) return true;
		else if (left.GetDay() < right.GetDay()) return false;
		if (left.GetHour() > right.GetHour()) return true;
		else if (left.GetHour() < right.GetHour()) return false;
		if (left.GetMinute() > right.GetMinute()) return true;
		else if (left.GetMinute() < right.GetMinute()) return false;
		if (left.GetSecond() > right.GetSecond()) return true;
		else if (left.GetSecond() < right.GetSecond()) return false;
		return true;
	}
	void Date::GetDate() { // вывод полей объекта на стандартный вывод
		std::cout << "GMT time: " << year << ":" << month << ":" << day << ":" << hour << ":" << minute << ":" << second << std::endl;
	}
	int Date::GetYear() const { // селектор года
		return this->year;
	}
	int Date::GetMonth() const { // селектор месяца
		return this->month;
	}
	int Date::GetDay() const { // селектор дня
		return this->day;
	}
	int Date::GetHour() const { // селектор часа
		return this->hour;
	}
	int Date::GetMinute() const { // селектор минуты
		return this->minute;
	}
	int Date::GetSecond() const { // селектор секунды
		return this->second;
	}
	DateInterval Date::GetInterval(const Date& another) const //интервал между двумя датами
	{
		int year =  another.GetYear() - GetYear();
		int month = (int)another.GetMonth() - (int)GetMonth();
		int day = another.GetDay() - GetDay();
		int hour = another.GetHour() - GetHour();
		int minute = another.GetMinute() - GetMinute();
		int second = another.GetSecond() - GetSecond();
		return DateInterval(year, month, day, hour, minute, second);
	}
	Date Date::addInterval(const DateInterval& interval) const
	{
		Date newDate(*this);
		newDate = newDate.addYears(interval.GetYearsInterval());
		newDate = newDate.addMonths(interval.GetMonthsInterval());
		newDate = newDate.addDays(interval.GetDaysInterval());
		newDate = newDate.addHours(interval.GetHoursInterval());
		newDate = newDate.addMinutes(interval.GetMinutesInterval());
		newDate = newDate.addSeconds(interval.GetSecondsInterval());
		return newDate;
	}
	std::string Date::monthName(Month mon)
	{
		if (mon == Jan)
			return "Jan";
		if (mon == Feb)
			return "Feb";
		if (mon == Mar)
			return "Mar";
		if (mon == Apr)
			return "Apr";
		if (mon == May)
			return "May";
		if (mon == Jun)
			return "Jun";
		if (mon == Jul)
			return "Jul";
		if (mon == Aug)
			return "Aug";
		if (mon == Sep)
			return "Sep";
		if (mon == Oct)
			return "Oct";
		if (mon == Nov)
			return "Nov";
		if (mon == Dec)
			return "Dec";
		return "ERROR";
	}
	std::string ReplaceString(std::string str, std::string patch, std::string digits)
	{
		int patch_size = patch.size();
		int pos = str.find(patch);
		if (pos != -1)
			str = str.replace(pos, patch_size, digits);
		return str;
	}
	std::string Date::formatDate(std::string format)
	{
		std::string str = format;
		str = ReplaceString(str, "YYYY", std::to_string(this->year));
		str = ReplaceString(str, "MMM", monthName(this->month));
		str = ReplaceString(str, "MM", std::to_string(this->month));
		str = ReplaceString(str, "DD", std::to_string(this->day));
		str = ReplaceString(str, "hh", std::to_string(this->hour));
		str = ReplaceString(str, "mm", std::to_string(this->minute));
		str = ReplaceString(str, "ss", std::to_string(this->second));
		return str;
	}
// (づ°ω°)づﾐe★゜・。。・゜゜・。。・゜☆゜・。。・゜゜・。。・゜