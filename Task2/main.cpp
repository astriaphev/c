﻿#include "Date.h"
#include "DateInterval.h"
#include "src\gtest-all.cc"

TEST(AddTimeTests, AddSeconds) {
	Date date = Date(0, 0, 0);
	ASSERT_EQ(0, date.GetSecond());
	ASSERT_EQ(0, date.GetMinute());
	date = date.addSeconds(5);
	ASSERT_EQ(5, date.GetSecond());
	ASSERT_EQ(0, date.GetMinute());
	date = date.addSeconds(65);
	ASSERT_EQ(10, date.GetSecond());
	ASSERT_EQ(1, date.GetMinute());
	date = date.addSeconds(-12);
	ASSERT_EQ(0, date.GetMinute());
	ASSERT_EQ(58, date.GetSecond());
}
TEST(AddTimeTests, AddMinutes) {
	Date date = Date(0, 0, 0);
	ASSERT_EQ(0, date.GetMinute());
	ASSERT_EQ(0, date.GetHour());
	ASSERT_EQ(0, date.GetSecond());
	date = date.addMinutes(240);
	ASSERT_EQ(0, date.GetMinute());
	ASSERT_EQ(4, date.GetHour());
	ASSERT_EQ(0, date.GetSecond());
	date = date.addMinutes(-241);
	ASSERT_EQ(23, date.GetHour());
	ASSERT_EQ(59, date.GetMinute());
	ASSERT_EQ(0, date.GetSecond());
}
TEST(AddTimeTests, AddHours) { //ТУТ ОШИБКА
	Date date = Date(1, Month::Jan, 1);
	date = date.addHours(5);
	ASSERT_EQ(1, date.GetDay());
	ASSERT_EQ(5, date.GetHour());
	date = date.addHours(20);
	ASSERT_EQ(2, date.GetDay());
	ASSERT_EQ(1, date.GetHour());
	date = date.addHours(-24);
	ASSERT_EQ(1, date.GetDay());
	ASSERT_EQ(1, date.GetHour());
}
TEST(AddTimeTests, AddDays) {
	Date date = Date(1, Month::Feb, 1);
	date = date.addDays(30);
	ASSERT_EQ(Month::Mar, date.GetMonth());
	ASSERT_EQ(3, date.GetDay());
	date = Date(4, Month::Feb, 1);
	date = date.addDays(30);
	ASSERT_EQ(2, date.GetDay());
	ASSERT_EQ(Month::Mar, date.GetMonth());

	date = Date(1, Month::Jan, 1);
	date = date.addDays(365);
	ASSERT_EQ(2, date.GetYear());
	ASSERT_EQ(Month::Jan, date.GetMonth());
	ASSERT_EQ(1, date.GetDay());
	date = Date(4, Month::Jan, 1);
	date = date.addDays(366);
	ASSERT_EQ(5, date.GetYear());
	ASSERT_EQ(Month::Jan, date.GetMonth());
	ASSERT_EQ(1, date.GetDay());
}
TEST(AddTimeTests, AddMonths) {
	Date date = Date(1, Month::Jan, 1);
	date = date.addMonths(25);
	ASSERT_EQ(3, date.GetYear());
	ASSERT_EQ(Month::Feb, date.GetMonth());
	ASSERT_EQ(1, date.GetDay());
}
TEST(AddTimeTests, UnmodifyingLogic) {
	Date date = Date(1, Month::Jan, 1, 0, 0, 0);
	ASSERT_EQ(1, date.GetYear());
	ASSERT_EQ(Month::Jan, date.GetMonth());
	ASSERT_EQ(1, date.GetDay());
	ASSERT_EQ(0, date.GetHour());
	ASSERT_EQ(0, date.GetMinute());
	ASSERT_EQ(0, date.GetSecond());
	date.addYears(1);
	date.addDays(1);
	date.addMonths(1);
	date.addHours(1);
	date.addMinutes(1);
	date.addSeconds(1);
	ASSERT_EQ(1, date.GetYear());
	ASSERT_EQ(Month::Jan, date.GetMonth());
	ASSERT_EQ(1, date.GetDay());
	ASSERT_EQ(0, date.GetHour());
	ASSERT_EQ(0, date.GetMinute());
	ASSERT_EQ(0, date.GetSecond());
	date = date.addYears(1);
	date = date.addDays(1);
	date = date.addMonths(1);
	date = date.addHours(1);
	date = date.addMinutes(1);
	date = date.addSeconds(1);
	ASSERT_EQ(2, date.GetYear());
	ASSERT_EQ(Month::Feb, date.GetMonth());
	ASSERT_EQ(2, date.GetDay());
	ASSERT_EQ(1, date.GetHour());
	ASSERT_EQ(1, date.GetMinute());
	ASSERT_EQ(1, date.GetSecond());
}
TEST(ConstructorsTests, CopyConstructors) {
	Date date = Date(2017, Month::Jan, 1, 0, 0, 0);
	ASSERT_EQ(2017, date.GetYear());
	ASSERT_EQ(Month::Jan, date.GetMonth());
	ASSERT_EQ(1, date.GetDay());

	Date dateSame = Date(2017, Month::Jan, 1);
	ASSERT_EQ(date.GetYear(), dateSame.GetYear());
	ASSERT_EQ(date.GetMonth(), dateSame.GetMonth());
	ASSERT_EQ(date.GetDay(), dateSame.GetDay());
	ASSERT_EQ(date.GetHour(), dateSame.GetHour());
	ASSERT_EQ(date.GetMinute(), dateSame.GetMinute());
	ASSERT_EQ(date.GetSecond(), dateSame.GetSecond());

	dateSame = Date(date);
	ASSERT_EQ(date.GetYear(), dateSame.GetYear());
	ASSERT_EQ(date.GetMonth(), dateSame.GetMonth());
	ASSERT_EQ(date.GetDay(), dateSame.GetDay());
	ASSERT_EQ(date.GetHour(), dateSame.GetHour());
	ASSERT_EQ(date.GetMinute(), dateSame.GetMinute());
	ASSERT_EQ(date.GetSecond(), dateSame.GetSecond());
}
TEST(ConstructorsTests, CurrentTimeConstructors) {
	Date date = Date();

	time_t rawtime;
	time(&rawtime);
	struct tm ptm;
	gmtime_s(&ptm, &rawtime);

	ASSERT_EQ(ptm.tm_year + 1900, date.GetYear());
	ASSERT_EQ((Month)(ptm.tm_mon + 1), date.GetMonth());
	ASSERT_EQ(ptm.tm_mday, date.GetDay());
	ASSERT_EQ(ptm.tm_hour, date.GetHour());
	ASSERT_EQ(ptm.tm_min, date.GetMinute());
	ASSERT_GE(ptm.tm_sec, date.GetSecond());

	date = Date(ptm.tm_hour, ptm.tm_min, ptm.tm_sec);
	ASSERT_EQ(ptm.tm_year + 1900, date.GetYear());
	ASSERT_EQ((Month)(ptm.tm_mon + 1), date.GetMonth());
	ASSERT_EQ(ptm.tm_mday, date.GetDay());
	ASSERT_EQ(ptm.tm_hour, date.GetHour());
	ASSERT_EQ(ptm.tm_min, date.GetMinute());
	ASSERT_EQ(ptm.tm_sec, date.GetSecond());
}
TEST(ConstructorsTests, WrongInitializing) {
	Date date = Date(2010, Month::Feb, 30, 22, 59, 72);
	ASSERT_EQ(2010, date.GetYear());
	ASSERT_EQ(Month::Mar, date.GetMonth());
	ASSERT_EQ(2, date.GetDay());
	ASSERT_EQ(23, date.GetHour());
	ASSERT_EQ(0, date.GetMinute());
	ASSERT_EQ(12, date.GetSecond());
}
TEST(GetIntervalTests, GetIntervalTests) {
	Date date1 = Date(1, Jan, 1, 0, 0, 0);
	Date date2 = date1;
	DateInterval interval = date1.GetInterval(date2);
	ASSERT_EQ(0, interval.GetYearsInterval());
	ASSERT_EQ(0, interval.GetMonthsInterval());
	ASSERT_EQ(0, interval.GetDaysInterval());
	ASSERT_EQ(0, interval.GetHoursInterval());
	ASSERT_EQ(0, interval.GetMinutesInterval());
	ASSERT_EQ(0, interval.GetSecondsInterval());

	date2 = date2.addYears(-1);
	interval = date1.GetInterval(date2);
	date1 = Date(1, Feb, 1);
	date2 = Date(1, Jan, 31);
	interval = date1.GetInterval(date2);
	ASSERT_EQ(0, interval.GetYearsInterval());
	ASSERT_EQ(-1, interval.GetMonthsInterval());
	ASSERT_EQ(30, interval.GetDaysInterval());
	date1 = date1.addInterval(interval);
	interval = date1.GetInterval(date2);
	ASSERT_EQ(0, interval.GetYearsInterval());
	ASSERT_EQ(0, interval.GetMonthsInterval());
	ASSERT_EQ(0, interval.GetDaysInterval());
}

int main(int argc, char** argv) {

	::testing::InitGoogleTest(&argc, argv);

	Date d1 = Date(); // current date
	Date d2 = Date(2007, Mar, 2, 15, 5, 0);

	// check constructor work
	assert(d2.GetDay() == 2);
	assert(d2.GetMonth() == Mar);
	assert(d2.GetHour() == 15);
	assert(d2.GetMinute() == 5);
	assert(d2.GetSecond() == 0);
	assert(d2.GetYear() == 2007);

	// check 'addXXX' methods
	assert(d2.addYears(1).GetYear() == d2.GetYear() + 1);
	assert(d2.addDays(-2).GetDay() == 28);
	assert(d2.addMonths(2).GetMonth() == May);
	assert(d2.addHours(24).GetHour() == d2.GetHour());
	assert(d2.addSeconds(-5).GetSecond() == 55);
	assert(d2.addMinutes(61).GetMinute() == 6);

	// check 'formatDate' method
	Date test(1982, Jul, 28, 14,59,60);
	std::string test_string = test.formatDate("Famous novel YYYY was written on MMM DDth somehow we know at hh:mm:ss, strange percise");
	std::string answer = "Famous novel 1982 was written on Jul 28th somehow we know at 15:0:0, strange percise";
	assert(test_string == answer);

	// check 'DateInterval' methods
	RUN_ALL_TESTS();
	system("pause");
	return 0;
}
// (づ°ω°)づﾐe★゜・。。・゜゜・。。・゜☆゜・。。・゜゜・。。・゜