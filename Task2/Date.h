﻿#ifndef DATE_H
#define DATE_H
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <time.h>
#include <iomanip>
#include <string>
class DateInterval;
enum Month {
	Jan = 1,
	Feb,
	Mar,
	Apr,
	May,
	Jun,
	Jul,
	Aug,
	Sep,
	Oct,
	Nov,
	Dec
};
class Date {
private:
	int year;
	Month month;
	int day;
	int hour;
	int minute;
	int second;
	void LimitDays(); // служебная функция
	void MinusNormalize(int delta, Date cpy, char mode); // нормализация для отрицательных значений
	void Normalize(int &year, Month &month, int &day, int &hour, int &minute, int &second); // исправление некорректных данных

public:
	Date(); // конструктор по умолчанию - текущая дата и время UTC
	Date(const Date &src); // конструктор копии
	Date(int year, Month month, int day, int hour, int minute, int second); // полный конструктор
	Date(unsigned int y, Month m, unsigned int d); // создает объект с временем 0 ч 0 м 0 сек
	Date(unsigned int hrs, unsigned int min, unsigned int sec); // создает объект с указанным временем и текущей датой
	Date addYears(int plus); // создание копии в котором увеличено количество лет
	Date addMonths(int plus); // создание копии в котором увеличено количество месяцев
	Date addDays(int plus); // создание копии в котором увеличено количество дней
	Date addHours(int plus); // создание копии в котором увеличено количество часов
	Date addMinutes(int plus); // создание копии в котором увеличено количество минут
	Date addSeconds(int plus); // создание копии в котором увеличено количество секунд
	std::string toString(); // возвращение строкового представления даты
	void GetDate(); // вывод полей объекта на стандартный вывод
	int GetYear() const; // селектор года
	int GetMonth() const; // селектор месяца
	int GetDay() const; // селектор дня
	int GetHour() const; // селектор часа
	int GetMinute() const; // селекто минуты
	int GetSecond() const; // селектор секунды
	DateInterval GetInterval(const Date& another) const; //интервал между текущей датой и another
	Date addInterval(const DateInterval & interval) const; //добавление интервала к текущей дате
	std::string Date::monthName(Month mon); //получение строкового представления месяца
	std::string formatDate(std::string format); //замена ключевых слов на компоненты текущей даты
};
bool operator>(Date& left, Date & right); //сравение дат
std::string ReplaceString(std::string str, std::string patch, std::string digits); //замена подстроки в строке
#endif
// (づ°ω°)づﾐe★゜・。。・゜゜・。。・゜☆゜・。。・゜゜・。。・゜