﻿#ifndef DATE_INTERVAL_H
#define DATE_INTERVAL_H
#define _CRT_SECURE_NO_WARNINGS

class DateInterval {
private:
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
public:
	DateInterval(); //инициализация по умолчанию
	DateInterval(int year, int month, int day, int hour, int minute, int second); //полный конструктор
	DateInterval(const DateInterval &interval); // конструктор копии
	int GetYearsInterval() const; //селектор интервала лет
	int GetMonthsInterval() const; //селектор интервала месяцев
	int GetDaysInterval() const; //селектор интервала дней
	int GetHoursInterval() const; //селектор интервала часов
	int GetMinutesInterval() const; //селектор интервала минут
	int GetSecondsInterval() const; //селектор интервала секунд
};
#endif
// (づ°ω°)づﾐe★゜・。。・゜゜・。。・゜☆゜・。。・゜゜・。。・゜