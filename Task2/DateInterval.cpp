﻿#include "DateInterval.h"

DateInterval::DateInterval() : //инициализация по умолчанию
	year(0),
	month(0),
	day(0),
	hour(0),
	minute(0),
	second(0)
{}
DateInterval::DateInterval(int year, int month, int day, int hour, int minute, int second) //полный конструктор
{
	this->year = year;
	this->month = month;
	this->day = day;
	this->hour = hour;
	this->minute = minute;
	this->second = second;
}
DateInterval::DateInterval(const DateInterval &interval) { // конструктор копии
	this->year = interval.year;
	this->month = interval.month;
	this->day = interval.day;
	this->hour = interval.hour;
	this->minute = interval.minute;
	this->second = interval.second;
}
int DateInterval::GetYearsInterval() const //селектор интервала лет
{
	return this->year;
}
int DateInterval::GetMonthsInterval() const //селектор интервала месяцев
{
	return month;
}
int DateInterval::GetDaysInterval() const //селектор интервала дней
{
	return day;
}
int DateInterval::GetHoursInterval() const //селектор интервала часов
{
	return hour;
}
int DateInterval::GetMinutesInterval() const //селектор интервала минут
{
	return minute;
}
int DateInterval::GetSecondsInterval() const //селектор интервала секунд
{
	return second;
}
// (づ°ω°)づﾐe★゜・。。・゜゜・。。・゜☆゜・。。・゜゜・。。・゜